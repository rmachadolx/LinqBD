﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqBD
{
    class ManipulaDados
    {
        static void Main2(string[] args)
        {

            ////MANIPULAÇÃO DE DADOS

            //DataClasses1DataContext dc = new DataClasses1DataContext();

            ////Inserir novo
            //Funcionario func = new Funcionario
            //{
            //    ID = dc.Funcionarios.Count() + 10,
            //    Nome = "Carlos Xavier",
            //    Departamento = "DF"
            //};

            //dc.Funcionarios.InsertOnSubmit(func);

            //try
            //{
            //    dc.SubmitChanges();
            //}
            //catch(Exception e)
            //{
            //    Console.WriteLine(e.Message);
            //}

            //var lista = from Funcionario in dc.Funcionarios select Funcionario;

            //foreach (Funcionario funcionario in lista)
            //{
            //    Console.WriteLine("ID: " + funcionario.ID);

            //    Console.WriteLine("Nome: " + funcionario.Nome);

            //    Console.WriteLine("Departamento: " + funcionario.Departamento);

            //    Console.WriteLine();

            //}

            //Console.WriteLine("Existem de momento {0} funcionários", lista.Count());


            //Console.WriteLine("--------------------------------ALTERAR-------------------------");

            ////Alterar
            //Funcionario funcionarioAAlterar = new Funcionario();

            //var pesquisa = from Funcionario in dc.Funcionarios
            //               where Funcionario.ID == 4
            //               select Funcionario;

            //funcionarioAAlterar = pesquisa.Single();
            //funcionarioAAlterar.Departamento = "RH";

            //try
            //{
            //    dc.SubmitChanges();
            //}
            //catch (Exception e)
            //{
            //    Console.WriteLine(e.Message);
            //}

            //var listaAlterada = from Funcionario in dc.Funcionarios select Funcionario;

            //foreach (Funcionario funcionario in listaAlterada)
            //{
            //    Console.WriteLine("ID: " + funcionario.ID);

            //    Console.WriteLine("Nome: " + funcionario.Nome);

            //    Console.WriteLine("Departamento: " + funcionario.Departamento);

            //    Console.WriteLine();

            //}

            //Console.WriteLine("--------------------------------ELIMINAR-------------------------");

            ////Eliminar
            //Funcionario f = new Funcionario();

            ////Depois de apagar vai dar Exception

            //var outraPesquisa = from Funcionario in dc.Funcionarios
            //                    where Funcionario.ID == 5
            //                    select Funcionario;

            //f = outraPesquisa.Single();

            //dc.Funcionarios.DeleteOnSubmit(f);

            //try
            //{
            //    dc.SubmitChanges();
            //}
            //catch(Exception e)
            //{
            //    Console.WriteLine(e.Message);
            //}

            //var listaApagada = from Funcionario in dc.Funcionarios select Funcionario;

            //foreach (Funcionario funcionario in listaApagada)
            //{
            //    Console.WriteLine("ID: " + funcionario.ID);

            //    Console.WriteLine("Nome: " + funcionario.Nome);

            //    Console.WriteLine("Departamento: " + funcionario.Departamento);

            //    Console.WriteLine();

           // }


            Console.ReadKey();


        }
    }
}
