﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqBD
{
    class Program2
    {
        static void Main3(string[] args)
        {
            DataClasses1DataContext dc = new DataClasses1DataContext();

            //Lista de funcionários que trabalham em DF
            //var lista = from Funcionario in dc.Funcionarios where Funcionario.Departamento == "DF" select Funcionario;

            //Funcionários com o nome começado por "Jo"
            //var lista = from Funcionario in dc.Funcionarios where Funcionario.Nome.StartsWith("Jo") select Funcionario;

            //Funcionários com nome "ana"
            //var lista = from Funcionario in dc.Funcionarios where Funcionario.Nome.Contains("ana") select Funcionario;

            //foreach (Funcionario func in lista)
            //{
            //    Console.WriteLine("ID: " + func.ID);

            //    Console.WriteLine("Nome: " + func.Nome);

            //    Console.WriteLine("Departamento: " + func.Departamento);

            //    Console.WriteLine();

            //}

            //Console.WriteLine("Existem de momento {0} funcionários", lista.Count());

            //Console.WriteLine("---------------------------------------------------------------");

            //int num = 3;
            ////Agrupar informação - Contar funcionários por departamento
            //var novaLista = from Funcionario in dc.Funcionarios
            //                group Funcionario by Funcionario.Departamento
            //                into c where c.Count() > num
            //                select new
            //                {
            //                    Departamento = c.Key,
            //                    Contagem = c.Count()
            //                };

            //if(novaLista.Count() >= 1)
            //{
            //    foreach (var c in novaLista)
            //    {
            //        Console.WriteLine(c.Departamento + "(" + c.Contagem + ")");
            //    }
            //}
            //else
            //{
            //    Console.WriteLine("Não existem");
            //}

            //Console.WriteLine("-----------------Junção de tabelas------------------");
            //var outraLista = from Funcionario in dc.Funcionarios
            //                 join Departamento in dc.Departamentos
            //                 on Funcionario.Departamento equals Departamento.Sigla
            //                 select new
            //                 {
            //                     Funcionario.ID,
            //                     Funcionario.Nome,
            //                     Departamento.Departamento1
            //                 };

            //foreach (var c in outraLista)
            //{
            //    Console.WriteLine("ID: " + c.ID);

            //    Console.WriteLine("Nome: " + c.Nome);

            //    Console.WriteLine("Departamento: " + c.Departamento1);

            //    Console.WriteLine();

            //}

            Console.ReadKey();


        }
    }
}

