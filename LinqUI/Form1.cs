﻿using LinqBD;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LinqUI
{
    public partial class Form1 : Form
    {

        DataClasses1DataContext dc = new DataClasses1DataContext();

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //Listview
            listView1.Columns.Add("Id");
            listView1.Columns.Add("Nome");
            listView1.Columns.Add("Departamento");

            //Carregar dados
            var lista = from Funcionario in dc.Funcionarios select Funcionario;

            foreach (Funcionarios func in lista)
            {
                ListViewItem item;

                item = listView1.Items.Add(func.ID.ToString());
                item.SubItems.Add(func.Nome);
                item.SubItems.Add(func.Departamento);
            }

            for (int idx = 0; idx <= 2; idx++)
            {
                listView1.Columns[idx].AutoResize(ColumnHeaderAutoResizeStyle.HeaderSize);
            }


            ////TREEVIEW
            //var outraLista = from Departamento in dc.Departamentos
            //                 select Departamento;


            //foreach(Departamento dep in outraLista)
            //{
            //    treeView1.Nodes.Add(dep.Sigla);
            //}

            ////segundo nível da árvore - Funcionários
            //var outraLista2 = from Funcionario in dc.Funcionarios
            //                  orderby Funcionario.Nome
            //                  select Funcionario;

            //string departamento;

            //foreach(Funcionario func in outraLista2)
            //{
            //    departamento = func.Departamento;

            //    foreach(TreeNode principal in treeView1.Nodes)
            //    {
            //        if(principal.Text == departamento)
            //        {
            //            principal.Nodes.Add(func.Nome);
            //        }
            //    }
            //}

            ////GridView
            //dataGridView1.Columns.Add("colId", "ID");
            //dataGridView1.Columns.Add("colNome", "Nome");
            //dataGridView1.Columns.Add("colDepartamento", "Departamento");

            //var outraLista3 = from Funcionario in dc.Funcionarios select Funcionario;

            //int idxlinha = 0;

            //DataGridViewCellStyle vermelho = new DataGridViewCellStyle();
            //vermelho.ForeColor = Color.Red;

            //foreach(Funcionario func in outraLista3)
            //{
            //    DataGridViewRow linha = new DataGridViewRow();
            //    dataGridView1.Rows.Add(linha);

            //    dataGridView1.Rows[idxlinha].Cells[0].Value = func.ID;
            //    dataGridView1.Rows[idxlinha].Cells[1].Value = func.Nome;
            //    dataGridView1.Rows[idxlinha].Cells[2].Value = func.Departamento;

            //    if((string)dataGridView1.Rows[idxlinha].Cells[2].Value == "DC")
            //    {
            //        dataGridView1.Rows[idxlinha].DefaultCellStyle = vermelho;
            //    }

            //    idxlinha++;
            //}

            //ajusta as colunas por código
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
        }
    }
}
