﻿namespace LinqCRUD
{
    using System;
    using System.Data;
    using System.Linq;
    using System.Windows.Forms;
    using LinqBD;
    public partial class FormMostrarDepartamento : Form
    {
        DataClasses1DataContext dc = new DataClasses1DataContext();
        public FormMostrarDepartamento()
        {
            InitializeComponent();

        }

        private void InitGrid()
        {
            ////GridView
            GridViewDepartamentos.Columns.Add("colSigla", "Sigla");

            GridViewDepartamentos.Columns.Add("colDepartamento", "Departamento");

            var lista = from departamento in dc.Departamentos select departamento;

            int idxlinha = 0;


            foreach (Departamentos dep in lista)
            {
                DataGridViewRow linha = new DataGridViewRow();

                GridViewDepartamentos.Rows.Add(linha);

                GridViewDepartamentos.Rows[idxlinha].Cells[0].Value = dep.Sigla;
                GridViewDepartamentos.Rows[idxlinha].Cells[1].Value = dep.Departamento;

                idxlinha++;


            }

            //ajusta as colunas por código
            GridViewDepartamentos.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
        }

        private void ButtonSair_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void FormMostrarDepartamento_Load(object sender, EventArgs e)
        {
            InitGrid();
        }
    }
}
