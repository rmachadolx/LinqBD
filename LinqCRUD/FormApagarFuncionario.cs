﻿

namespace LinqCRUD
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Forms;
    using LinqBD;
    public partial class FormApagarFuncionario : Form
    {
        DataClasses1DataContext dc = new DataClasses1DataContext();
        public FormApagarFuncionario()
        {
            InitializeComponent();
            Limpar(this.Controls);
            InitCombo();

        }

        private void InitCombo()
        {

            var lista = from funcionario in dc.Funcionarios
                        select funcionario;

            ComboBoxFuncionarios.Items.Add("<Selecione o funcionário>");

            ComboBoxFuncionarios.DataSource = lista;
            ComboBoxFuncionarios.DisplayMember = "Nome";
            ComboBoxFuncionarios.ValueMember = "ID";

            ComboBoxFuncionarios.SelectedIndex = -1;
            
        }

        private void ButtonApagarFuncionario_Click(object sender, EventArgs e)
        {
            if (ComboBoxFuncionarios.SelectedIndex == -1)
            {
                MessageBox.Show("Selecione o funcionário");
                ComboBoxFuncionarios.Focus();
            }
            else
            {
                Funcionarios func = new Funcionarios();

                var Pesquisa = from funcionario in dc.Funcionarios
                               where funcionario.ID == (int)ComboBoxFuncionarios.SelectedValue
                               select funcionario;

                func = Pesquisa.Single();
                dc.Funcionarios.DeleteOnSubmit(func);

                try
                {
                    dc.SubmitChanges();
                    MessageBox.Show("Funcionário eliminado com sucesso !");
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
                Limpar(this.Controls);
                InitCombo();
            }
        }
        private void Limpar(Control.ControlCollection controles)
        {
            foreach (Control ctrl in controles)
            {
                //Se o contorle for um TextBox...
                if (ctrl is TextBox)
                {
                    ((TextBox)(ctrl)).Text = String.Empty;
                }
                if (ctrl is ComboBox)
                {
                    ((ComboBox)(ctrl)).SelectedIndex = -1;
                }
            }

        }
    }
}
