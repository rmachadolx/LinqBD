﻿namespace LinqCRUD
{
    partial class FormApagarDepartamento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ComboBoxDepartamento = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.ButtonApagarDepartamento = new System.Windows.Forms.Button();
            this.ButtonSair = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ComboBoxDepartamento
            // 
            this.ComboBoxDepartamento.FormattingEnabled = true;
            this.ComboBoxDepartamento.Items.AddRange(new object[] {
            "Selecione Departamento"});
            this.ComboBoxDepartamento.Location = new System.Drawing.Point(291, 89);
            this.ComboBoxDepartamento.Name = "ComboBoxDepartamento";
            this.ComboBoxDepartamento.Size = new System.Drawing.Size(194, 21);
            this.ComboBoxDepartamento.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(163, 89);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(110, 16);
            this.label1.TabIndex = 1;
            this.label1.Text = "Departamento:";
            // 
            // ButtonApagarDepartamento
            // 
            this.ButtonApagarDepartamento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonApagarDepartamento.Location = new System.Drawing.Point(291, 157);
            this.ButtonApagarDepartamento.Name = "ButtonApagarDepartamento";
            this.ButtonApagarDepartamento.Size = new System.Drawing.Size(194, 23);
            this.ButtonApagarDepartamento.TabIndex = 2;
            this.ButtonApagarDepartamento.Text = "Apagar Departamento";
            this.ButtonApagarDepartamento.UseVisualStyleBackColor = true;
            this.ButtonApagarDepartamento.Click += new System.EventHandler(this.ButtonApagarDepartamento_Click);
            // 
            // ButtonSair
            // 
            this.ButtonSair.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonSair.Location = new System.Drawing.Point(688, 208);
            this.ButtonSair.Name = "ButtonSair";
            this.ButtonSair.Size = new System.Drawing.Size(75, 23);
            this.ButtonSair.TabIndex = 8;
            this.ButtonSair.Text = "Sair";
            this.ButtonSair.UseVisualStyleBackColor = true;
            this.ButtonSair.Click += new System.EventHandler(this.ButtonSair_Click);
            // 
            // FormApagarDepartamento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(814, 261);
            this.Controls.Add(this.ButtonSair);
            this.Controls.Add(this.ButtonApagarDepartamento);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ComboBoxDepartamento);
            this.Name = "FormApagarDepartamento";
            this.Text = " Apagar Departamento";
            this.Load += new System.EventHandler(this.FormApagarDepartamento_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox ComboBoxDepartamento;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button ButtonApagarDepartamento;
        private System.Windows.Forms.Button ButtonSair;
    }
}