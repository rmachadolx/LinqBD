﻿namespace LinqCRUD
{
    using System;
    using System.Windows.Forms;
    using LinqBD;
    public partial class FormAdicionarDepartamento : Form
    {

        DataClasses1DataContext dc = new DataClasses1DataContext();
        public FormAdicionarDepartamento()
        {
            InitializeComponent();
            Limpar(Controls);
        }

        private void ButtonAdicionarDepartamento_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(TextBoxSigla.Text))
            {
                MessageBox.Show("campo de preenchimento obrigatório");
                TextBoxSigla.Focus();
            }
            else if (string.IsNullOrEmpty(TextBoxDepartamento.Text))
            {
                MessageBox.Show("campo de preenchimento obrigatório");
                TextBoxDepartamento.Focus();
            }

            else
            {
                Departamentos departamento = new Departamentos
                {
                    Sigla = TextBoxSigla.Text,
                    Departamento = TextBoxDepartamento.Text
                };

                dc.Departamentos.InsertOnSubmit(departamento);
                try
                {
                    dc.SubmitChanges();
                    MessageBox.Show("Departamento Inserido com sucesso!");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                Limpar(this.Controls);
            }

        }

        private void Limpar(Control.ControlCollection controles)
        {
            foreach (Control ctrl in controles)
            {
                //Se o contorle for um TextBox...
                if (ctrl is TextBox)
                {
                    ((TextBox)(ctrl)).Text = String.Empty;
                }
            }

        }
        private void ButtonSair_Click(object sender, EventArgs e)
        {
            this.Hide();
        }


    }
}
