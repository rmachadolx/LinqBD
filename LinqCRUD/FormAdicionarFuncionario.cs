﻿namespace LinqCRUD
{
    using System;
    using System.Data;
    using System.Linq;
    using System.Windows.Forms;
    using LinqBD;
    public partial class FormAdicionarFuncionario : Form
    {
        DataClasses1DataContext dc = new DataClasses1DataContext();
        public FormAdicionarFuncionario()
        {
            InitializeComponent();
            Limpar(this.Controls);
            GerarID();
            InitCombo();
           

        }

        private void GerarID()
        {
            var pesquisa = (from funcionario in dc.Funcionarios
                            orderby funcionario.ID descending
                            select funcionario.ID).First();

            LabelID.Text = (pesquisa + 1).ToString();

        }

        private void InitCombo()
        {
           
      
            var lista = from dep in dc.Departamentos select dep;
            ComboBoxDepartamentos.Items.Add("<Selecione o departamento>");

            foreach (Departamentos dep in lista)
            {
                ComboBoxDepartamentos.Items.Add(dep.Sigla);
            }
            ComboBoxDepartamentos.SelectedIndex= 0;
            ComboBoxDepartamentos.ValueMember = "Sigla";
        }

        private void ButtonAdicionarFuncionario_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(TextBoxNome.Text))
            {
                MessageBox.Show("campo de preenchimento obrigatório");
                TextBoxNome.Focus();
            }
            else if (ComboBoxDepartamentos.SelectedIndex<1)
            {
                MessageBox.Show("Selecione o departamento");
                ComboBoxDepartamentos.Focus();
            }
            else
            {
        Funcionarios funcionario = new Funcionarios
            {
                ID = Convert.ToInt32(LabelID.Text),
                Nome = TextBoxNome.Text,
                Departamento = (ComboBoxDepartamentos.SelectedItem.ToString())

            };

            dc.Funcionarios.InsertOnSubmit(funcionario);
            try
            {
                dc.SubmitChanges();
                MessageBox.Show("Departamento Inserido com sucesso!");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
                Limpar(this.Controls);
                InitCombo();
                GerarID();
            }
    
        }
        private void Limpar(Control.ControlCollection controles)
        {
            foreach (Control ctrl in controles)
            {
                //Se o contorle for um TextBox...
                if (ctrl is TextBox)
                {
                    ((TextBox)(ctrl)).Text = String.Empty;
                }
                if (ctrl is ComboBox)
                {
                    ((ComboBox)(ctrl)).SelectedIndex = -1;
                }
            }
          
        }
     

        private void ButtonSair_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
    }
}
