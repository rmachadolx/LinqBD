﻿namespace LinqCRUD
{
    using System;
    using System.Data;
    using System.Drawing;
    using System.Linq;
    using System.Windows.Forms;
    using LinqBD;

    public partial class FormMostrarFuncionario : Form
    {
        DataClasses1DataContext dc = new DataClasses1DataContext();
        public FormMostrarFuncionario()
        {
            InitializeComponent();
            InitGrid();
        }

        private void InitGrid()
        {
            ////GridView
            GridViewFuncionarios.Columns.Add("colId", "ID");
            GridViewFuncionarios.Columns.Add("colNome", "Nome");
            GridViewFuncionarios.Columns.Add("colDepartamento", "Departamento");

            var lista = from funcionario in dc.Funcionarios select funcionario;

            int idxlinha = 0;

            foreach (Funcionarios func in lista)
            {
                DataGridViewRow linha = new DataGridViewRow();
                GridViewFuncionarios.Rows.Add(linha);

                GridViewFuncionarios.Rows[idxlinha].Cells[0].Value = func.ID;
                GridViewFuncionarios.Rows[idxlinha].Cells[1].Value = func.Nome;
                GridViewFuncionarios.Rows[idxlinha].Cells[2].Value = func.Departamento;

                idxlinha++;
            }

            //ajusta as colunas por código
            GridViewFuncionarios.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
        }

        private void ButtonSair_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

    }
}
