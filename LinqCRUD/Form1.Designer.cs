﻿namespace LinqCRUD
{
    partial class Form1
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label1 = new System.Windows.Forms.Label();
            this.ComboBoxTabelas = new System.Windows.Forms.ComboBox();
            this.ButtonAdicionar = new System.Windows.Forms.Button();
            this.ButtonMostrar = new System.Windows.Forms.Button();
            this.ButtonApagar = new System.Windows.Forms.Button();
            this.ButtonAlterar = new System.Windows.Forms.Button();
            this.ButtonSair = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(43, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(143, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Selecione a tabela:";
            // 
            // ComboBoxTabelas
            // 
            this.ComboBoxTabelas.FormattingEnabled = true;
            this.ComboBoxTabelas.Location = new System.Drawing.Point(192, 25);
            this.ComboBoxTabelas.Name = "ComboBoxTabelas";
            this.ComboBoxTabelas.Size = new System.Drawing.Size(147, 21);
            this.ComboBoxTabelas.TabIndex = 1;
            // 
            // ButtonAdicionar
            // 
            this.ButtonAdicionar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonAdicionar.Location = new System.Drawing.Point(46, 90);
            this.ButtonAdicionar.Name = "ButtonAdicionar";
            this.ButtonAdicionar.Size = new System.Drawing.Size(121, 23);
            this.ButtonAdicionar.TabIndex = 2;
            this.ButtonAdicionar.Text = "Adicionar";
            this.ButtonAdicionar.UseVisualStyleBackColor = true;
            this.ButtonAdicionar.Click += new System.EventHandler(this.ButtonAdicionar_Click);
            // 
            // ButtonMostrar
            // 
            this.ButtonMostrar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonMostrar.Location = new System.Drawing.Point(218, 90);
            this.ButtonMostrar.Name = "ButtonMostrar";
            this.ButtonMostrar.Size = new System.Drawing.Size(121, 23);
            this.ButtonMostrar.TabIndex = 3;
            this.ButtonMostrar.Text = "Mostrar";
            this.ButtonMostrar.UseVisualStyleBackColor = true;
            this.ButtonMostrar.Click += new System.EventHandler(this.ButtonMostrar_Click);
            // 
            // ButtonApagar
            // 
            this.ButtonApagar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonApagar.Location = new System.Drawing.Point(218, 167);
            this.ButtonApagar.Name = "ButtonApagar";
            this.ButtonApagar.Size = new System.Drawing.Size(121, 23);
            this.ButtonApagar.TabIndex = 4;
            this.ButtonApagar.Text = "Apagar";
            this.ButtonApagar.UseVisualStyleBackColor = true;
            this.ButtonApagar.Click += new System.EventHandler(this.ButtonApagar_Click);
            // 
            // ButtonAlterar
            // 
            this.ButtonAlterar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonAlterar.Location = new System.Drawing.Point(46, 167);
            this.ButtonAlterar.Name = "ButtonAlterar";
            this.ButtonAlterar.Size = new System.Drawing.Size(121, 23);
            this.ButtonAlterar.TabIndex = 5;
            this.ButtonAlterar.Text = "Alterar";
            this.ButtonAlterar.UseVisualStyleBackColor = true;
            this.ButtonAlterar.Click += new System.EventHandler(this.ButtonAlterar_Click);
            // 
            // ButtonSair
            // 
            this.ButtonSair.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonSair.Image = ((System.Drawing.Image)(resources.GetObject("ButtonSair.Image")));
            this.ButtonSair.Location = new System.Drawing.Point(370, 228);
            this.ButtonSair.Name = "ButtonSair";
            this.ButtonSair.Size = new System.Drawing.Size(49, 48);
            this.ButtonSair.TabIndex = 6;
            this.ButtonSair.UseVisualStyleBackColor = true;
            this.ButtonSair.Click += new System.EventHandler(this.ButtonSair_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(431, 288);
            this.ControlBox = false;
            this.Controls.Add(this.ButtonSair);
            this.Controls.Add(this.ButtonAlterar);
            this.Controls.Add(this.ButtonApagar);
            this.Controls.Add(this.ButtonMostrar);
            this.Controls.Add(this.ButtonAdicionar);
            this.Controls.Add(this.ComboBoxTabelas);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Empresa";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox ComboBoxTabelas;
        private System.Windows.Forms.Button ButtonAdicionar;
        private System.Windows.Forms.Button ButtonMostrar;
        private System.Windows.Forms.Button ButtonApagar;
        private System.Windows.Forms.Button ButtonAlterar;
        private System.Windows.Forms.Button ButtonSair;
    }
}

