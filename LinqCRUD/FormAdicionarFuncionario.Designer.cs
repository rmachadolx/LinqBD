﻿namespace LinqCRUD
{
    partial class FormAdicionarFuncionario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormAdicionarFuncionario));
            this.label1 = new System.Windows.Forms.Label();
            this.LabelID = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.TextBoxNome = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.ComboBoxDepartamentos = new System.Windows.Forms.ComboBox();
            this.ButtonAdicionarFuncionario = new System.Windows.Forms.Button();
            this.ButtonSair = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(144, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "ID:";
            // 
            // LabelID
            // 
            this.LabelID.AutoSize = true;
            this.LabelID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelID.Location = new System.Drawing.Point(177, 51);
            this.LabelID.Name = "LabelID";
            this.LabelID.Size = new System.Drawing.Size(23, 16);
            this.LabelID.TabIndex = 1;
            this.LabelID.Text = "ID";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(118, 103);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "Nome:";
            // 
            // TextBoxNome
            // 
            this.TextBoxNome.Location = new System.Drawing.Point(180, 99);
            this.TextBoxNome.Name = "TextBoxNome";
            this.TextBoxNome.Size = new System.Drawing.Size(244, 20);
            this.TextBoxNome.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(52, 158);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(110, 16);
            this.label3.TabIndex = 4;
            this.label3.Text = "Departamento:";
            // 
            // ComboBoxDepartamentos
            // 
            this.ComboBoxDepartamentos.FormattingEnabled = true;
            this.ComboBoxDepartamentos.Location = new System.Drawing.Point(180, 157);
            this.ComboBoxDepartamentos.Name = "ComboBoxDepartamentos";
            this.ComboBoxDepartamentos.Size = new System.Drawing.Size(246, 21);
            this.ComboBoxDepartamentos.TabIndex = 5;
            // 
            // ButtonAdicionarFuncionario
            // 
            this.ButtonAdicionarFuncionario.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonAdicionarFuncionario.Image = ((System.Drawing.Image)(resources.GetObject("ButtonAdicionarFuncionario.Image")));
            this.ButtonAdicionarFuncionario.Location = new System.Drawing.Point(180, 226);
            this.ButtonAdicionarFuncionario.Name = "ButtonAdicionarFuncionario";
            this.ButtonAdicionarFuncionario.Size = new System.Drawing.Size(51, 44);
            this.ButtonAdicionarFuncionario.TabIndex = 6;
            this.ButtonAdicionarFuncionario.Tag = "Adicionar Funcionário";
            this.toolTip1.SetToolTip(this.ButtonAdicionarFuncionario, "Adiconar funcionário");
            this.ButtonAdicionarFuncionario.UseVisualStyleBackColor = true;
            this.ButtonAdicionarFuncionario.Click += new System.EventHandler(this.ButtonAdicionarFuncionario_Click);
            // 
            // ButtonSair
            // 
            this.ButtonSair.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonSair.Image = ((System.Drawing.Image)(resources.GetObject("ButtonSair.Image")));
            this.ButtonSair.Location = new System.Drawing.Point(550, 286);
            this.ButtonSair.Name = "ButtonSair";
            this.ButtonSair.Size = new System.Drawing.Size(51, 44);
            this.ButtonSair.TabIndex = 8;
            this.ButtonSair.Tag = "Sair";
            this.toolTip1.SetToolTip(this.ButtonSair, "Sair");
            this.ButtonSair.UseVisualStyleBackColor = true;
            this.ButtonSair.Click += new System.EventHandler(this.ButtonSair_Click);
            // 
            // FormAdicionarFuncionario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(613, 342);
            this.ControlBox = false;
            this.Controls.Add(this.ButtonSair);
            this.Controls.Add(this.ButtonAdicionarFuncionario);
            this.Controls.Add(this.ComboBoxDepartamentos);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.TextBoxNome);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.LabelID);
            this.Controls.Add(this.label1);
            this.Name = "FormAdicionarFuncionario";
            this.Text = "Adicionar Funcionário";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label LabelID;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TextBoxNome;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox ComboBoxDepartamentos;
        private System.Windows.Forms.Button ButtonAdicionarFuncionario;
        private System.Windows.Forms.Button ButtonSair;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}