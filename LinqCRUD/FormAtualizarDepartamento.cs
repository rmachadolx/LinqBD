﻿namespace LinqCRUD
{
    using System;
    using System.Data;
    using System.Linq;
    using System.Windows.Forms;
    using LinqBD;

    public partial class FormAtualizarDepartamento : Form
    {
        DataClasses1DataContext dc = new DataClasses1DataContext();

        public FormAtualizarDepartamento()
        {
            InitializeComponent();
            InitCombo();
        }

        private void InitCombo()
        {
            var lista = from departamento in dc.Departamentos
                        select departamento;
            ComboBoxSigla.DataSource = lista;
            ComboBoxSigla.DisplayMember = "Sigla";
            ComboBoxSigla.ValueMember = "Sigla";
            ComboBoxSigla.SelectedIndex = -1;
        }

        private void ButtonAtuliazarDepartamento_Click(object sender, EventArgs e)
        {
            Departamentos depart = new Departamentos();

            var Pesquisa = from dep in dc.Departamentos
                           where dep.Sigla == ComboBoxSigla.SelectedValue.ToString()
                           select dep;

            depart = Pesquisa.Single();
            depart.Departamento = TextBoxDepartamento.Text;

            try
            {
                dc.SubmitChanges();
                MessageBox.Show("Departamento alterado com sucesso !");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            this.Hide();
        }

        private void ComboBoxSigla_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ComboBoxSigla.SelectedIndex!=-1)
            {
    Departamentos depart = new Departamentos();

            var Pesquisa = from dep in dc.Departamentos
                           where dep.Sigla == ComboBoxSigla.SelectedValue.ToString()
                           select dep;

            depart = Pesquisa.Single();
           TextBoxDepartamento.Text= depart.Departamento ;
            }
        
        }
    }
}
