﻿namespace LinqCRUD
{
    partial class FormMostrarDepartamento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMostrarDepartamento));
            this.ButtonSair = new System.Windows.Forms.Button();
            this.GridViewDepartamentos = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.GridViewDepartamentos)).BeginInit();
            this.SuspendLayout();
            // 
            // ButtonSair
            // 
            this.ButtonSair.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonSair.Image = ((System.Drawing.Image)(resources.GetObject("ButtonSair.Image")));
            this.ButtonSair.Location = new System.Drawing.Point(309, 314);
            this.ButtonSair.Name = "ButtonSair";
            this.ButtonSair.Size = new System.Drawing.Size(49, 48);
            this.ButtonSair.TabIndex = 0;
            this.ButtonSair.Tag = "Sair";
            this.ButtonSair.UseVisualStyleBackColor = true;
            this.ButtonSair.Click += new System.EventHandler(this.ButtonSair_Click);
            // 
            // GridViewDepartamentos
            // 
            this.GridViewDepartamentos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GridViewDepartamentos.Location = new System.Drawing.Point(68, 32);
            this.GridViewDepartamentos.Name = "GridViewDepartamentos";
            this.GridViewDepartamentos.Size = new System.Drawing.Size(534, 241);
            this.GridViewDepartamentos.TabIndex = 1;
            // 
            // FormMostrarDepartamento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(670, 416);
            this.ControlBox = false;
            this.Controls.Add(this.GridViewDepartamentos);
            this.Controls.Add(this.ButtonSair);
            this.Name = "FormMostrarDepartamento";
            this.Text = " Mostra Departamento";
            this.Load += new System.EventHandler(this.FormMostrarDepartamento_Load);
            ((System.ComponentModel.ISupportInitialize)(this.GridViewDepartamentos)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button ButtonSair;
        private System.Windows.Forms.DataGridView GridViewDepartamentos;
    }
}