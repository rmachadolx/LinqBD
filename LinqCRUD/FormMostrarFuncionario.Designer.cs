﻿namespace LinqCRUD
{
    partial class FormMostrarFuncionario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMostrarFuncionario));
            this.GridViewFuncionarios = new System.Windows.Forms.DataGridView();
            this.ButtonSair = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.GridViewFuncionarios)).BeginInit();
            this.SuspendLayout();
            // 
            // GridViewFuncionarios
            // 
            this.GridViewFuncionarios.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GridViewFuncionarios.Location = new System.Drawing.Point(28, 33);
            this.GridViewFuncionarios.Name = "GridViewFuncionarios";
            this.GridViewFuncionarios.Size = new System.Drawing.Size(414, 237);
            this.GridViewFuncionarios.TabIndex = 0;
            // 
            // ButtonSair
            // 
            this.ButtonSair.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonSair.Image = ((System.Drawing.Image)(resources.GetObject("ButtonSair.Image")));
            this.ButtonSair.Location = new System.Drawing.Point(203, 329);
            this.ButtonSair.Name = "ButtonSair";
            this.ButtonSair.Size = new System.Drawing.Size(49, 48);
            this.ButtonSair.TabIndex = 8;
            this.ButtonSair.Tag = "Sair";
            this.ButtonSair.UseVisualStyleBackColor = true;

            // 
            // FormMostrarFuncionario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(468, 458);
            this.Controls.Add(this.ButtonSair);
            this.Controls.Add(this.GridViewFuncionarios);
            this.Name = "FormMostrarFuncionario";
            this.Text = "Mostrar Funcionário";
            ((System.ComponentModel.ISupportInitialize)(this.GridViewFuncionarios)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView GridViewFuncionarios;
        private System.Windows.Forms.Button ButtonSair;
    }
}