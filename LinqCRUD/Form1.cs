﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LinqCRUD
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            ComboBoxTabelas.Items.Add("<Selecione a tabela>");
            ComboBoxTabelas.Items.Add("Departamentos");
            ComboBoxTabelas.Items.Add("Funcionários");
            ComboBoxTabelas.SelectedIndex =0;
            
        }
        private FormAdicionarDepartamento FormAddDepart = new FormAdicionarDepartamento();
        private FormAdicionarFuncionario FormAddFunc = new FormAdicionarFuncionario();

        private FormMostrarDepartamento FormShowDepart = new FormMostrarDepartamento();
        private FormMostrarFuncionario FormShowFunc = new FormMostrarFuncionario();

        private FormAtualizarDepartamento FormUpdateDepart = new FormAtualizarDepartamento();

        private FormApagarDepartamento FormDeleteDepart = new FormApagarDepartamento();
        private FormApagarFuncionario FormDeleteFunc = new FormApagarFuncionario();

        //ComboboBox.Items.Add("<Selecione o departamento>");

        private void ButtonAdicionar_Click(object sender, EventArgs e)
        {
            if (ComboBoxTabelas.SelectedIndex >0)
            {


                if (ComboBoxTabelas.SelectedItem.ToString() == "Departamentos")
                {
                    FormAddDepart.Show();
                }
                else
                {
                    FormAddFunc.Show();
                }
            }
            else
            {
                MessageBox.Show("Selecione uma Tabela");
                ComboBoxTabelas.Focus();
            }
        }

        private void ButtonMostrar_Click(object sender, EventArgs e)
        {
            if (ComboBoxTabelas.SelectedIndex >0 )
            {

                if (ComboBoxTabelas.SelectedItem.ToString() == "Departamentos")
                {
                    FormShowDepart.Show();
                }
                else
                {
                    FormShowFunc.Show();
                }
            }
            else
            {
                MessageBox.Show("Selecione uma Tabela");
                ComboBoxTabelas.Focus();
            }
        }

        private void ButtonAlterar_Click(object sender, EventArgs e)
        {
            if (ComboBoxTabelas.SelectedIndex >0)
            {

                if (ComboBoxTabelas.SelectedItem.ToString() == "Departamentos")
                {
                    FormUpdateDepart.Show();
                }
                else
                {
               //      FormUpdaateFunc.Show();
                }
            }
            else
            {
                MessageBox.Show("Selecione uma Tabela");
                ComboBoxTabelas.Focus();
            }

        }

        private void ButtonApagar_Click(object sender, EventArgs e)
        {
            if (ComboBoxTabelas.SelectedIndex >0)
            {

                if (ComboBoxTabelas.SelectedItem.ToString() == "Departamentos")
                {
                    FormDeleteDepart.Show();
                }
                else
                {
                    FormDeleteFunc.Show();
                }
            }
            else
            {
                MessageBox.Show("Selecione uma Tabela");
                ComboBoxTabelas.Focus();
            }
        }

        private void ButtonSair_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
