﻿namespace LinqCRUD
{
    partial class FormApagarFuncionario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ComboBoxFuncionarios = new System.Windows.Forms.ComboBox();
            this.ButtonApagarFuncionario = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // ComboBoxFuncionarios
            // 
            this.ComboBoxFuncionarios.FormattingEnabled = true;
            this.ComboBoxFuncionarios.Location = new System.Drawing.Point(143, 54);
            this.ComboBoxFuncionarios.Name = "ComboBoxFuncionarios";
            this.ComboBoxFuncionarios.Size = new System.Drawing.Size(363, 21);
            this.ComboBoxFuncionarios.TabIndex = 0;
            // 
            // ButtonApagarFuncionario
            // 
            this.ButtonApagarFuncionario.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonApagarFuncionario.Location = new System.Drawing.Point(143, 160);
            this.ButtonApagarFuncionario.Name = "ButtonApagarFuncionario";
            this.ButtonApagarFuncionario.Size = new System.Drawing.Size(157, 23);
            this.ButtonApagarFuncionario.TabIndex = 1;
            this.ButtonApagarFuncionario.Text = "Apagar Funcionário";
            this.ButtonApagarFuncionario.UseVisualStyleBackColor = true;
            this.ButtonApagarFuncionario.Click += new System.EventHandler(this.ButtonApagarFuncionario_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(84, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 16);
            this.label1.TabIndex = 2;
            this.label1.Text = "Nome:";
            // 
            // FormApagarFuncionario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(605, 252);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ButtonApagarFuncionario);
            this.Controls.Add(this.ComboBoxFuncionarios);
            this.Name = "FormApagarFuncionario";
            this.Text = "Apagar Funcionario";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox ComboBoxFuncionarios;
        private System.Windows.Forms.Button ButtonApagarFuncionario;
        private System.Windows.Forms.Label label1;
    }
}