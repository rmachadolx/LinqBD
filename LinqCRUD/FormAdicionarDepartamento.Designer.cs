﻿namespace LinqCRUD
{
    partial class FormAdicionarDepartamento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormAdicionarDepartamento));
            this.label1 = new System.Windows.Forms.Label();
            this.TextBoxSigla = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.TextBoxDepartamento = new System.Windows.Forms.TextBox();
            this.ButtonSair = new System.Windows.Forms.Button();
            this.ButtonAdicionarDepartamento = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(143, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Sigla:";
            // 
            // TextBoxSigla
            // 
            this.TextBoxSigla.Location = new System.Drawing.Point(222, 49);
            this.TextBoxSigla.Name = "TextBoxSigla";
            this.TextBoxSigla.Size = new System.Drawing.Size(100, 20);
            this.TextBoxSigla.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(81, 127);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(110, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "Departamento:";
            // 
            // TextBoxDepartamento
            // 
            this.TextBoxDepartamento.Location = new System.Drawing.Point(222, 123);
            this.TextBoxDepartamento.Name = "TextBoxDepartamento";
            this.TextBoxDepartamento.Size = new System.Drawing.Size(282, 20);
            this.TextBoxDepartamento.TabIndex = 3;
            // 
            // ButtonSair
            // 
            this.ButtonSair.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonSair.Image = ((System.Drawing.Image)(resources.GetObject("ButtonSair.Image")));
            this.ButtonSair.Location = new System.Drawing.Point(627, 234);
            this.ButtonSair.Name = "ButtonSair";
            this.ButtonSair.Size = new System.Drawing.Size(49, 48);
            this.ButtonSair.TabIndex = 7;
            this.ButtonSair.UseVisualStyleBackColor = true;
            this.ButtonSair.Click += new System.EventHandler(this.ButtonSair_Click);
            // 
            // ButtonAdicionarDepartamento
            // 
            this.ButtonAdicionarDepartamento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonAdicionarDepartamento.Image = ((System.Drawing.Image)(resources.GetObject("ButtonAdicionarDepartamento.Image")));
            this.ButtonAdicionarDepartamento.Location = new System.Drawing.Point(222, 191);
            this.ButtonAdicionarDepartamento.Name = "ButtonAdicionarDepartamento";
            this.ButtonAdicionarDepartamento.Size = new System.Drawing.Size(49, 48);
            this.ButtonAdicionarDepartamento.TabIndex = 4;
            this.ButtonAdicionarDepartamento.Tag = "Adicionar Departamento";
            this.ButtonAdicionarDepartamento.UseVisualStyleBackColor = true;
            this.ButtonAdicionarDepartamento.Click += new System.EventHandler(this.ButtonAdicionarDepartamento_Click);
            // 
            // FormAdicionarDepartamento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(688, 294);
            this.ControlBox = false;
            this.Controls.Add(this.ButtonSair);
            this.Controls.Add(this.ButtonAdicionarDepartamento);
            this.Controls.Add(this.TextBoxDepartamento);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.TextBoxSigla);
            this.Controls.Add(this.label1);
            this.Name = "FormAdicionarDepartamento";
            this.Text = " Adicionar Departamento";
          
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TextBoxSigla;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TextBoxDepartamento;
        private System.Windows.Forms.Button ButtonAdicionarDepartamento;
        private System.Windows.Forms.Button ButtonSair;
    }
}