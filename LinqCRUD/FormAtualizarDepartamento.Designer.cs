﻿namespace LinqCRUD
{
    partial class FormAtualizarDepartamento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.ComboBoxSigla = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.TextBoxDepartamento = new System.Windows.Forms.TextBox();
            this.ButtonAtuliazarDepartamento = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(212, 56);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Sigla:";
            // 
            // ComboBoxSigla
            // 
            this.ComboBoxSigla.FormattingEnabled = true;
            this.ComboBoxSigla.Location = new System.Drawing.Point(276, 51);
            this.ComboBoxSigla.Name = "ComboBoxSigla";
            this.ComboBoxSigla.Size = new System.Drawing.Size(121, 21);
            this.ComboBoxSigla.TabIndex = 1;
            this.ComboBoxSigla.SelectedIndexChanged += new System.EventHandler(this.ComboBoxSigla_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(150, 102);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(110, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "Departamento:";
            // 
            // TextBoxDepartamento
            // 
            this.TextBoxDepartamento.Location = new System.Drawing.Point(276, 97);
            this.TextBoxDepartamento.Name = "TextBoxDepartamento";
            this.TextBoxDepartamento.Size = new System.Drawing.Size(201, 20);
            this.TextBoxDepartamento.TabIndex = 3;
            // 
            // ButtonAtuliazarDepartamento
            // 
            this.ButtonAtuliazarDepartamento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonAtuliazarDepartamento.Location = new System.Drawing.Point(276, 177);
            this.ButtonAtuliazarDepartamento.Name = "ButtonAtuliazarDepartamento";
            this.ButtonAtuliazarDepartamento.Size = new System.Drawing.Size(201, 23);
            this.ButtonAtuliazarDepartamento.TabIndex = 4;
            this.ButtonAtuliazarDepartamento.Text = "Atualizar Departamento";
            this.ButtonAtuliazarDepartamento.UseVisualStyleBackColor = true;
            this.ButtonAtuliazarDepartamento.Click += new System.EventHandler(this.ButtonAtuliazarDepartamento_Click);
            // 
            // FormAtualizarDepartamento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(703, 261);
            this.Controls.Add(this.ButtonAtuliazarDepartamento);
            this.Controls.Add(this.TextBoxDepartamento);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.ComboBoxSigla);
            this.Controls.Add(this.label1);
            this.Name = "FormAtualizarDepartamento";
            this.Text = " Atualizar Departamento";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox ComboBoxSigla;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TextBoxDepartamento;
        private System.Windows.Forms.Button ButtonAtuliazarDepartamento;
    }
}