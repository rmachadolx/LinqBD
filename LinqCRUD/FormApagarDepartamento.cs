﻿
namespace LinqCRUD
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Forms;
    using LinqBD;

    public partial class FormApagarDepartamento : Form
    {
        DataClasses1DataContext dc = new DataClasses1DataContext();
        public FormApagarDepartamento()
        {
            InitializeComponent();


        }

        private void InitCombo()
        {
            var lista = from departamento in dc.Departamentos
                        select departamento;

  
            ComboBoxDepartamento.DataSource = lista;
            ComboBoxDepartamento.DisplayMember = "Departamento";
            ComboBoxDepartamento.ValueMember = "Sigla";
          
        }

        private void ButtonApagarDepartamento_Click(object sender, EventArgs e)
        {
            Departamentos dep = new Departamentos();

            ////Depois de apagar vai dar Exception

            var Pesq = from departamento in dc.Departamentos
                       where departamento.Sigla == ComboBoxDepartamento.SelectedValue.ToString()
                       select departamento;

            dep = Pesq.Single();

            dc.Departamentos.DeleteOnSubmit(dep);

            try
            {
                dc.SubmitChanges();
                MessageBox.Show("Departamento eliminado com sucesso !");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            Limpar(this.Controls);
        }
        private void Limpar(Control.ControlCollection controles)
        {
            foreach (Control ctrl in controles)
            {
                //Se o contorle for um TextBox...
                if (ctrl is TextBox)
                {
                    ((TextBox)(ctrl)).Text = String.Empty;
                }
                if (ctrl is ComboBox)
                {
                    ((ComboBox)(ctrl)).SelectedIndex = -1;
                    InitCombo();
                }
            }

        }
        private void ButtonSair_Click(object sender, EventArgs e)
        {
          this.Hide();
        }

        private void FormApagarDepartamento_Load(object sender, EventArgs e)
        {
            InitCombo();
        }
    }
}
